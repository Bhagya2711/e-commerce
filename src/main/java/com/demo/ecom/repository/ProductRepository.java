package com.demo.ecom.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.ecom.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {

}
